//soal release 0

class Animal {
    constructor(name){
        this.name = name 
        this.legs = 4
        this.cold_blooded = false
    }
}

var shaun = new Animal('bees');

console.log(shaun.name)
console.log(shaun.legs)
console.log(shaun.cold_blooded)


//soal release 2
console.log('===============')

class Ape{
    constructor(name){
        this.name = name
        this.legs = 2
        this.yell = "Auooo"
    }
}

var sungokong = new Ape('kera sakti');

console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.yell)

class Frog{
    constructor(name){
        this.name = name
        this.legs = 4
        this.jump = "hop hop"
    }
}

var kodok = new Frog('buduk');

console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.jump)


//soal no 2
console.log('================')
class Clock{
    constructor(template){
        var timer;

  function render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    var output = template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  this.stop = function() {
    clearInterval(timer);
  };

  this.start = function() {
    render();
    timer = setInterval(render, 1000);
  };

  var clock = new Clock({template: 'h:m:s'});
  clock.start();

    }
}

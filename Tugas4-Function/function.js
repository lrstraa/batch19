//Tugas 1
function teriak(greet, kode, simbol){
    return greet + kode + simbol;
}
var greet = "Halo";
var kode = " Sanbers";
var simbol = "!"

var hasilTeriak = teriak(greet, kode, simbol);
console.log(hasilTeriak)

//Tugas 2
console.log('==========================')

function kalikan(num1, num2){
    return num1 * num2;
}

var hasilKali = kalikan(12, 4);
console.log(hasilKali);

//Tugas 3
console.log('========================')

function awal(name, age, address, hobby){
    return "Nama saya " + name + ", " + "umur saya " + age + " tahun, " + "alamat saya di " + address + "," + " dan saya punya hobby yaitu " + hobby + "!"
}

var name = "Riri";
var age = 22;
var address = "Pancoran";
var hobby = "Bersepeda";

var introduction = awal(name, age, address, hobby);
console.log(introduction);

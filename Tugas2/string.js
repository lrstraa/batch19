//tugas 1 
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log(word.concat(" ").concat(second).concat(" ").concat(third).concat(" ").concat(fourth).concat(" ").concat(fifth).concat(" ").concat(sixth).concat(" ").concat(seventh))

//tugas 2
var sentences = "I am going to be React Native Developer"; 

var firstSentences = sentences[0];
var secondSentences = sentences[2] + sentences[3];
var thirdSentences = sentences[5] + sentences [6] + sentences [7] + sentences [8] + sentences[9];
var fourthSentences = sentences[11] + sentences[12];
var fifthSentences = sentences[14] + sentences[15];
var sixthSentences = sentences[17] + sentences[18] + sentences[19] + sentences[20] + sentences[21];
var seventhSentences = sentences[23] + sentences[24] + sentences[25] +sentences[26] + sentences[27] + sentences[28];
var eighthSentences = sentences[30] + sentences[31] + sentences[32] +sentences[33] + sentences[34] + sentences[35] + sentences[36] + sentences[37] + sentences[38];

console.log("First word: " + firstSentences)
console.log("Second word: " + secondSentences)
console.log("Third word: " + thirdSentences)
console.log("Fourth word: " + fourthSentences)
console.log("Fifth word: " + fifthSentences)
console.log("Six word: " + sixthSentences)
console.log("Seventh word: " + seventhSentences)
console.log("Eight word: " + eighthSentences)


//soal 3
var sentences2 = 'wow JavaScript is so cool'; 
var firstWord2 = sentences2.substring(0, 3);
var secondWord2 = sentences2.substring(4, 13);
var thirdWord2 = sentences2.substring(15, 17);
var fourthWord2 = sentences2.substring(18, 20);
var fifthWord2 = sentences2.substring(21);

console.log('First Word: ' + firstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);

//soal 4
var sentences3 = 'wow JavaScript is so cool'; 

var firstWord3 = sentences3.substring(0, 3); 
var secondWord3 = sentences3.substring(4, 13);
var thirdWord3 = sentences3.substring(15, 17);
var fourthWord3 = sentences3.substring(18, 20);
var fifthWord3 = sentences3.substring(21);

var firstWordLength = firstWord3.length 
var secondWordLength = secondWord3.length 
var thirdWordLength = thirdWord3.length 
var fourthWordLength = fourthWord3.length 
var fifthWordLength = fifthWord3.length 

console.log('First Word: ' + firstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 

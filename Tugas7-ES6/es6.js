//soal 1

let golden = goldenFunction = () => {
    console.log('this is golden!')
}

goldenFunction();


//soal 2
console.log('================')

let newFunction = literalFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () =>{
            console.log(firstName + ' ' + lastName)
            return fullName
        }
    }
}

newFunction('William', "Imoh")

//soal 3
console.log('================')

let newObject = {
    firstName1: "Harry",
    lastName2: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const {firstName1, lastName2, destination, occupation, spell} = newObject;

console.log(firstName1);
console.log(spell);

//soal 4
console.log('=================')

let west = ['Will', 'Chris', 'Sam', 'Holly']
let east = ['Gill', 'Brian', 'Noel', 'Maggie']

let combineCompass = [...west, ...east]
console.log(combineCompass)

//soal 5
console.log('=================')
const planet = "earth"
const view = "glass"
let before = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam`


console.log(before)